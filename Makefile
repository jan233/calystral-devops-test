export BINARY_NAME=program

build:
	go build -o ${BINARY_NAME}

run:
	go build -o ${BINARY_NAME} && ./${BINARY_NAME} && rm ${BINARY_NAME}

test:
	go fmt $(go list ./... | grep -v /vendor/)
	go vet $(go list ./... | grep -v /vendor/)
	go test -coverprofile=coverage-report.out ./...
	go tool cover -html=coverage-report.out -o coverage-report.html
	go tool cover -func=coverage-report.out