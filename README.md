### Quickly set-up your dev-environment with VS Code and Docker 
If you are using Visual Studio Code and have Docker installed into your host OS, just use the "Reopen in container" action from the VS Code remote-containers. Your environment will be opened inside a docker container with all the project denepndencies installed.

### Runing and testing

Make sure to have Make installed in your environment (inside our dev-container it will be!).
Use `make test` for running unit tests or `make run` for running the program. 

### Contributing
Make the necessary modifications at a new branch and test it at your machine. Once it's ready for production, open a Merge Request to the master branch. 
When creating a Merge Request, make sure to fill all the requested informations at the template.
Before merging aninthing into master, it is important to check the pipeline executed at Merge Request and ensure the test coverage is as high as expected and all tests are passing. 